import { GoogleCloudPage } from "../pageobjects/pages/googlecloud.page.js";
import { SearchResultsPage } from "../pageobjects/pages/gcsearchresults.page.js";
import { PricingCalculatorPage } from "../pageobjects/pages/calculator.page.js";
import { MailDax } from "../pageobjects/pages/maildax.page.js";

const googlecloudPage = new GoogleCloudPage();
const searchResultsPage = new SearchResultsPage();
const calculatorPage = new PricingCalculatorPage();
const mailPage = new MailDax();

describe('Google cloud legacy calculator tests', () => {
  before(async () => {
    await browser.maximizeWindow();
    await googlecloudPage.open();
  })

  it('Verify Google Cloud page loaded', async () => {
    await expect(browser).toHaveTitle('Servicios de cloud computing | Google Cloud');
  })

  it('Search for Google Cloud Platform Pricing Calculator and select legacy result', async () => {
    const searchTerm = 'Google Cloud Platform Pricing Calculator';
    await googlecloudPage.Header.search.setValue(searchTerm);
    await browser.keys('Enter');
    await expect(browser).toHaveTitle(expect.stringContaining(searchTerm));

    await searchResultsPage.calculatorLegacy.click();
    await expect(browser).toHaveUrl('https://cloud.google.com/products/calculator-legacy');

  })

  it('Fill out Google Cloud Pricing Calculator', async () => {
    //navigate into page iframes
    await calculatorPage.enterMainFrame();
    //select number of instances: 4
    await calculatorPage.mainForm.instances.click();
    await calculatorPage.mainForm.instances.setValue(4);
    
    //select free option of OS
    await calculatorPage.mainForm.operatingSystem.click();
    await calculatorPage.mainForm.systems('free').click();
    const osDescription = 'Free: Debian, CentOS, CoreOS, Ubuntu or BYOL (Bring Your Own License)';
    await expect(calculatorPage.mainForm.systemsLabel).toHaveText(osDescription);
    
    //select provisioning model: regular
    await calculatorPage.mainForm.provisioningModel.waitForDisplayed();
    await calculatorPage.mainForm.provisioningModel.click();
    await calculatorPage.mainForm.selectVM('regular').waitForDisplayed();
    await calculatorPage.mainForm.selectVM('regular').click();
    await expect(calculatorPage.mainForm.provisioningLabel).toHaveText('Regular');
    
    //select machine family: general purpose
    await calculatorPage.mainForm.machineFamily.click();
    await calculatorPage.mainForm.selectFamily('general').waitForDisplayed();
    await calculatorPage.mainForm.selectFamily('general').click();
    await expect(calculatorPage.mainForm.machineLabel).toHaveText('General purpose');
    
    // select machine series: n1
    await calculatorPage.mainForm.series.click();
    await calculatorPage.mainForm.selectSeries('n1').waitForDisplayed();
    await calculatorPage.mainForm.selectSeries('n1').click();
    await expect(calculatorPage.mainForm.seriesLabel).toHaveText('N1');

    // select machine series n1-standard-8
    await calculatorPage.mainForm.machineTypeLabel.click();
    await calculatorPage.mainForm.machineType('standard 8').waitForDisplayed();
    await calculatorPage.mainForm.machineType('standard 8').click();
    await expect(calculatorPage.mainForm.machineTypeLabel).toHaveText('n1-standard-8 (vCPUs: 8, RAM: 30GB)');

    //add 1 gpu,  NVIDIA Tesla T4 (this is available at desired location)
    await calculatorPage.mainForm.gpuCheckbox.scrollIntoView();
    await calculatorPage.mainForm.gpuCheckbox.click();
    await calculatorPage.mainForm.gpuType.click();
    await calculatorPage.mainForm.gpuSelection('t4').waitForDisplayed();
    await calculatorPage.mainForm.gpuSelection('t4').click();
    await calculatorPage.mainForm.gpuCount.waitForDisplayed();
    await calculatorPage.mainForm.gpuCount.click();
    await calculatorPage.mainForm.gpuNum(1).waitForDisplayed();
    await calculatorPage.mainForm.gpuNum(1).click();
    await expect(calculatorPage.mainForm.gpuLabel).toHaveText('NVIDIA Tesla T4');
    await calculatorPage.mainForm.gpuNumLabel.waitForDisplayed();
    await expect(calculatorPage.mainForm.gpuNumLabel).toHaveText('1');

    //datacenter location: Frankfurt (europe-west3)
    await calculatorPage.mainForm.location.scrollIntoView();
    await calculatorPage.mainForm.location.click();
    await calculatorPage.mainForm.locationInput.setValue('europe-west3');
    await calculatorPage.mainForm.locationResult.waitForDisplayed();
    await calculatorPage.mainForm.locationResult.click();

    // selected commited usage 1 year
    await calculatorPage.mainForm.usage.click();
    await calculatorPage.mainForm.usageYears(1).waitForDisplayed();
    await calculatorPage.mainForm.usageYears(1).click();

    // click add estimate
    await calculatorPage.mainForm.addEstimateButton.click();

    // get the total cost from the estimate. used later for comparison
    const cartCost = await calculatorPage.Estimate.cartCost.getText();
    
    // open new window and navigate to maildax website.
    await browser.newWindow('https://maildax.com', {windowName: 'MailDax'});

    // get the randomly generated email and save it.
    const email = await mailPage.email.getText();

    // switch back to google legacy calculator page
    const handles = await browser.getWindowHandles();
    await browser.switchToWindow(handles[0]);
    
    // access the iframes again
    await browser.switchToFrame(0);
    await browser.switchToFrame(0);

    // click the send by email button and wait for email form component
    await calculatorPage.Estimate.emailButton.click();
    await calculatorPage.mailForm.rootEl.waitForDisplayed();

    await calculatorPage.mailForm.firstName.setValue('Ignacio');
    await calculatorPage.mailForm.lastName.setValue('Perez');
    await calculatorPage.mailForm.email.setValue(email);
    await calculatorPage.mailForm.sendEmailButton.click();

    await browser.switchToWindow(handles[1]);
    await mailPage.inbox.latestMail.waitForDisplayed();
    await mailPage.inbox.latestMail.click();
    const quoteText = await $('//td[2]//h3').getText();

    await expect(cartCost).toContain(quoteText);

  })

})