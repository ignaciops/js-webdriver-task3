export class InboxComponent {

  get latestMail() {
    return $('div.absolute.top-0.left-0.w-full.h-full.z-0');
  }
}