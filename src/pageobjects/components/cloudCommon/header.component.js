export class HeaderComponent {
  get rootEl () {
    return $('header')
  }
  
  get search () {
    return this.rootEl.$('input')
  }

}