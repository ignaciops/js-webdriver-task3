export class MainFormComponent {
  get rootEl() {
    return $('#mainForm');
  }

  get instances() {
    return $('[ng-model="listingCtrl.computeServer.quantity"]');
  }

  get instanceUse() {
    return $('[ng-model="listingCtrl.computeServer.label"]');
  }

  get operatingSystem() {
    return $('[ng-model="listingCtrl.computeServer.os"]');
  }

  systems(param) {
    const selectors = {
      'free': 'md-option[id="select_option_102"]',
      'ubuntu-pro': '[id="select_option_103"]'
    }
    return $(`${selectors[param.toLowerCase()]}`);
  }

  get systemsLabel() {
    return $('[id="select_value_label_92"] .md-text');
  }

  get provisioningModel() {
    return $('[ng-model="listingCtrl.computeServer.class"]');
  }

  selectVM(param) {
    const selectors = {
      'regular': '[id="select_option_115"]',
      'spot': '[id="select_option_116"]'
    }
    return $(`${selectors[param.toLowerCase()]}`);
  }

  get provisioningLabel() {
    return $('[id="select_value_label_93"] .md-text');
  }

  get machineFamily() {
    return $('[ng-model="listingCtrl.computeServer.family"]');
  }

  selectFamily(param) {
    const selectors = {
      'general': '[id="select_option_119"]',
      'compute':'[id="select_option_120"]',
      'memory': '[id="select_option_121"]'
    }
    return $(`${selectors[param.toLowerCase()]}`);
  }

  get machineLabel() {
    return $('[id="select_value_label_94"] .md-text');
  }

  get series() {
    return $('[id="select_value_label_95"]');
  }

  selectSeries(param) {
    const selectors = {
      'n1': '[id="select_option_224"]',
      'n2': '[id="select_option_225"]',
      'e2': '[id="select_option_226"]',
      'n2d': '[id="select_option_227"]',
      't2a': '[id="select_option_228"]',
      't2d': '[id="select_option_229"]',
    }
    return $(`${selectors[param.toLowerCase()]}`);
  }

  get seriesLabel() {
    return $('[id="select_value_label_95"] .md-text');
  }

  get machineTypeLabel() {
    return $('[id="select_value_label_96"] .md-text');
  }

  machineType(param) {
    const selectors = {
      'standard 1': '[id="select_option_471"]',
      'standard 2': '[id="select_option_472"]',
      'standard 4': '[id="select_option_473"]',
      'standard 8': '[id="select_option_474"]',
    }
    return $(`${selectors[param.toLowerCase()]}`);
  }

  get gpuCheckbox() {
    return $('[ng-model="listingCtrl.computeServer.addGPUs"]');
  }

  get gpuType() {
    return $('[ng-model="listingCtrl.computeServer.gpuType"]');
  }

  gpuSelection(param) {
    const selectors = {
      'k80': '[value="NVIDIA_TESLA_K80"]',
      'p100': '[value="NVIDIA_TESLA_P100"]',
      'p4': '[value="NVIDIA_TESLA_P4"]',
      'v100': '[value="NVIDIA_TESLA_V100"]',
      't4': '[value="NVIDIA_TESLA_T4"]',
    }
    return $(`${selectors[param.toLowerCase()]}`);
  }

  get gpuCount() {
    return $('[ng-model="listingCtrl.computeServer.gpuCount"]');
  }

  get gpuLabel() {
    return $('[id="select_value_label_508"] .md-text');
  }

  get gpuNumLabel() {
    return $('[id="select_value_label_509"] .md-text');
  }

  gpuNum(param) {
    const selectors = {
      1: '//md-option[@ng-disabled="item.value != 0 && item.value < listingCtrl.minGPU"][2]',
      2: '//md-option[@ng-disabled="item.value != 0 && item.value < listingCtrl.minGPU"][3]',
      3: '//md-option[@ng-disabled="item.value != 0 && item.value < listingCtrl.minGPU"][4]',
      4: '//md-option[@ng-disabled="item.value != 0 && item.value < listingCtrl.minGPU"][5]',
    }
    return $(`${selectors[param]}`);
  }

  get storage() {
    return $('[ng-model="listingCtrl.computeServer.ssd"]');
  }
  
  selectStorage(param) {
    const selectors = {
      '1x375': '[id="select_option_494"]',
      '2x375': '[id="select_option_495"]',
      '3x375': '[id="select_option_496"]',
      '4x375': '[id="select_option_497"]',
      '5x375': '[id="select_option_498"]',
    }
    return $(`${selectors[param.toLowerCase()]}`);
  }

  get location() {
    return $('[ng-model="listingCtrl.computeServer.location"]');
  }

  get locationInput() {
    return $('[ng-model="listingCtrl.inputRegionText.computeServer"]');
  }

  get locationResult() {
    return $('[ng-repeat="item in listingCtrl.fullRegionList | filter:listingCtrl.inputRegionText.computeServer"');
  }

  get usage() {
    return $('[ng-model="listingCtrl.computeServer.cud"]');
  }

  usageYears(param) {
    const selectors = {
      0: '//md-option[@id="select_option_137"]',
      1: '//md-option[@id="select_option_138"]',
      3: '//md-option[@id="select_option_139"]'
    }
    return $(`${selectors[param]}`);
  }

  get addEstimateButton() {
    return $('//div[@ng-if="listingCtrl.showComputeEngine"]//button[contains(text(),"Add to Estimate")]');
  }
}