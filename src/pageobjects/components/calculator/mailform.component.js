export class MailForm {
  get rootEl() {
    return $('//form[@name="emailForm"]');
  }

  get firstName() {
    return $('[ng-model="emailQuote.user.firstname"]');
  }

  get lastName() {
    return $('[ng-model="emailQuote.user.lastname"]');
  }

  get email() {
    return $('[ng-model="emailQuote.user.email"]');
  }

  get sendEmailButton() {
    return $('//button[contains(text(),"Send Email")]');
  }

}