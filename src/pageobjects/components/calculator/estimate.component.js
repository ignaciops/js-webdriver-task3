export class EstimateComponent {
  
  get cartCost() {
    return $('//div[@class="cpc-cart-total"]//b');
  }

  get emailButton() {
    return $('//span[text()="email"]');
  }

}