import { HeaderComponent } from "../components/cloudCommon/header.component.js"

export class GoogleCloudPage {
  constructor () {
    this.Header = new HeaderComponent()
  }

  async open() {
    await browser.url('https://cloud.google.com')
  }

} 