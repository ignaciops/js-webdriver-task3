export class SearchResultsPage {
  get rootEl() {
    return $('article.devsite-article')
  }

  get calculatorLegacy() {
    return $('//div[@class="gs-title"]//a[@class="gs-title" and contains(@href, "calculator-legacy")]')
  }
}