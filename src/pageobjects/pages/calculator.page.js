import { MainFormComponent} from "../components/calculator/options.component.js"
import { EstimateComponent } from "../components/calculator/estimate.component.js";
import { MailForm } from "../components/calculator/mailform.component.js";

export class PricingCalculatorPage {
  constructor() {
    this.mainForm = new MainFormComponent();
    this.Estimate = new EstimateComponent();
    this.mailForm = new MailForm();
  }

  async enterMainFrame() {
    await browser.switchToFrame(0)
    await browser.switchToFrame(0)
  }

}