import { InboxComponent } from "../components/maildax/inbox.component.js";

export class MailDax {
  constructor () {
    this.inbox = new InboxComponent();
  }
  
  get email() {
    return $('//div[contains(@class, "mt-2")]//span');
  }
}